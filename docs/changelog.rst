History
=======

.. note ::

   The changelog is produced by adding the following call into the post-commit hook ::
   
       git log --no-merges --format=format:"%s%n-------------%n.. codeauthor:: %aN <%aE>%n:Date:%aI%n%n%b%n%N" > CHANGELOG.rst

.. include :: ../CHANGELOG.rst