.. Nose Job (Sphinx) documentation master file, created by
   sphinx-quickstart on Tue Sep 26 02:57:39 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

==========================
Sphinx-Contrib. : Nose Job
==========================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :includehidden:
   :glob:

   Install      <install>
   Usage        <usage>

   .. Docutils     <docutils>
   .. Sphinx       <sphinx>
   .. Nose Job     <nosejob>

   Development  <development>
   F.A.Q.       <faq>
   Glossary     <glossary>
   Change Log   <changelog>

.. .. include :: ../README.rst

------------
Introduction
------------

|Project| provides additional functionality and features that one felt were missing from Sphinx.

.. |Project| provides a testing ground for such functionality and features that one would like to eventually contribute back into the Sphinx project.
.. It is not meant to be distributed in its present form but it intended to useful as is.
.. The following features were the ones one felt were missing from either Sphinx or the 3rd party packages.

  :ref:`API Doc <nosejob.apidoc>`
    This superceeds the :command:`sphinx-apidoc` package
  :ref:`API Doc <nosejob.epilog>`
    This provides a common set of terms typical of Python projects.

-----
Setup
-----

Users may use Python's package installer pip to manage the installation, upgrade and removal of :mod:`nosejob`.

Configure
=========

`Configuration <https://www.sphinx-doc.org/en/master/usage/configuration.html>`_ is done as usual within :file:`docs/conf.py`.

.. .. only :: builder_html
.. 
..   .. include :: ../readme.rst

------------------
Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
