--------
Packages
--------

Sphinx Contribution Packages
============================

This is a selection of Packages floating about the internet that should probbly be merged into nosejob.

:mod:`P3`
---------

`P3 <https://github.com/pattern-languages/sphinxcontrib-p3>`_ provides graphvix integration.

Unit Testing
============

Sphinx extensions may be tested using the stock Python testing module in conjunction with :ref:`sphinx-testing`.
The Sphinx sources use :ref:`pytest` instead for their unit testing, I haven't quite worked out how they do it though.

.. _sphinx-testing:

:mod:`sphinx-testing`
---------------------

This is used for unit testing ones sphinx contribution packages; usually for testing directives and roles.
`sphinx-testing <https://github.com/sphinx-doc/sphinx-testing>`_ provides a contextual decorator that instantiates Sphinx to build a given set of documentation.
The tests generally use some form of parser to ensure that the output is generated properly.

.. autodoc:: sphinx-testing.with_app

.. _pytest:

:mod:`PyTest`
-------------

This is used within the Sphinx sources which decorate the respective testing functions with a `PyTest <https://github.com/>`_ marker.
What is unclear here is how the decorator injects a Sphinx builder into the test.

:mod:`sphinx-test-reports`
--------------------------

`Sphinx Test Reporting <https://pypi.org/project/sphinx-test-reports/>`_ for reporting on ones unit tests.
