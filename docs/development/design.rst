------
Design
------

The respective features are maintained as separate sub-packages or sub-modules.
This allows one to list `sphinxcontrib.nosejob`, to include all NoseJob `FEATURES`, or repeatedly list `sphinxcontrib.nosejob.FEATURE`, to include one or more the `FEATURE(S)`, in their sphinx configuration file :file:`source/conf.py`

Sub-packages are free to be structured however the sub-package Author sees fit.
Logically there are only two structures that should result however.
The first splits a sub-package into its functional componenets e.g. domain, directives, roles, nodes.
The second provides further sub-features.
