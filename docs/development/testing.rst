-------
Testing
-------

Docutils relies upon some odd methodologies for `unit testing <http://docutils.sourceforge.net/docs/dev/testing.html>`_.
Sphinx uses :mod:`pytest` internally with which I'm still gaining familiarity.
This package tries to stick with the stock :mod:`unittest` module.
