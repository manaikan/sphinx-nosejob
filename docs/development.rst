-----
Notes
-----

.. toctree::
   :maxdepth: 3
   :caption: Contents:
   :includehidden:

   Packages     <development/packages>

..   Machinery    <development/machinery>
..   Formats      <development/formats>
..   Models       <development/models>
..   Themes       <development/themes>
..   Integration  <development/integration>

   Testing      <development/testing>
   Errors       <development/errors>

..   Tools        <tools>
..   Utilities    <utils>
..   Tests        <tests>
