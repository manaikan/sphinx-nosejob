------------
Installation
------------

Standard Installation
=====================

The easiest means of installing the **Apeman:Overlay(s)** package is to invoke :command:`pip install` as follows
::

  pip install sphinx-nosejob

Source Based Installation
=========================

.. program:: git clone

Download the source code from the repository and run ``pip`` from the project root as follows
::

    git clone <REPOSITORY>
    cd <FODLER>
    pip install -e .

Upgrade
=======

.. program:: pip install

.. option:: --upgrade
   :hide:

To upgrade the package invoke :command:`pip` with the :option:`--upgrade <pip install --upgrade>` command line switch as follows
::

    pip install --upgrade sphinx-nosejob

Remove
======

.. program:: pip uninstall

One may remove the package by invoking :command:`pip` one last time with the :command:`pip uninstall` sub-command as follows
::

  pip uninstall sphinx-nosejob


