-------
Queries 
-------

|RsT|
=====

 :Q: Is it possible to reference other pars of a document from a literal block ?

 :A: `:code:`parsed-literal` <http://docutils.sourceforge.net/FAQ.html#how-can-i-make-a-literal-block-with-some-formatting>`_ directives may be used to process and generate annotations in code sources

     .. parsed-literal::

        print "Hello world!"  # *tricky* `code <http://docutils.sourceforge.net/FAQ.html#how-can-i-make-a-literal-block-with-some-formatting>`_

Documentation
=============

 :Q: How was the API documentation generated

 :A: |Sphinx| and |Project| both provide an `apidoc` script that will traverse the packages and modules under a project to generate the necessary API documentation.

     * For |Docutils| this was invokes as follows ::

         python -m nosejob.apidoc -e docs/* -e test/* -e tools/* -e setup.py -e install.py -gv E:\Python\docutils\docutils docs docutils

     * For Nose Job the following was used ::

         python -m nosejob.apidoc -e docs/* -e text/* -e "docs - Copy/*" -e setup.py -e dist/* -gv nosejob docs nosejob

     * For Sphinx the following was used ::

         python -m nosejob.apidoc -e doc/* -e tests/* -e utils/* -e setup.py -gv E:\Python\sphinx docs sphinx
