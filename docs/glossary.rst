--------
Glossary
--------

.. glossary::

 Grouping
  The sutomodule references are placed within the package document
  rather then in a standalone document.

 Element
  An atomic component within a document model.
  While this is avoiaded as much as possible it is interchangeable with "node" in the docutils vernacular, "tag" in |HTML|, "box" in |TeXs|
