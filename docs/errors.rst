------
Errors
------

Directives
==========

The following occurs wehn ones directive is not setup to accept any content.
::

    WARNING: Error in "tree" directive: no content permitted.

Consider converting the directive into a role or setting.
Users should simply unindent the imediately subsequent block as Sphinx interpreting this as the directives' content.
