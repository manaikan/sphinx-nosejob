#!/usr/bin/env python
"""
Develop
-------

To setup a development installation one may call either pip as 
follows,
::
   pip install -e .
 
or execute the setup script directly.
::
   python setup.py develop

The pip version is generally preferred.

Document
--------

Documentation may be generated using the setup script.
::
   python setup.py build_sphinx -b singlehtml|html|latex

Select between singleHTML, HTMl and LaTeX as necessary.
The output is saved under :file:`dist/html|latex`.
"""
import setuptools
# from sphinxcontrib.nosejob import __meta__

setuptools.setup(
    # setup_requires=['pbr>=1.9', 'setuptools>=17.1'],
    # pbr=True,
    command_options      = {'build_sphinx' : {
                            # 'version'      : ('setup.py', ".".join([str(v) for v in __meta__.__version__])),
                            # 'release'      : ('setup.py', ".".join([str(v) for v in __meta__.__release__])),
                            'source_dir'   : ('setup.py','docs'),
                            'build_dir'    : ('setup.py','dist'),
                            'config_dir'   : ('setup.py','docs'),}},
    # use_scm_version=True,
)


# from distutils.core import setup
# from setuptools import find_packages
# from nosejob import __meta__
# import os

# setup(
#  name                 = 'Sphinx-Nose-Job',
#  version              = __meta__.version(), 
#  description          = 'Sphinx patches and other utilities useful in rhinoplasty',
#  long_description     = open(os.path.join(os.path.dirname(__file__), 'README.md')).read(),
#  url                  = 'http://www.manaikan.com/',
#  author               = 'Carel van Dam',
#  author_email         = 'carelvdam@gmail.com',
#  license              = 'To Be Decided',
#  platforms            = 'any',
#  install_requires     = ['apeman','apeman-overlays'],
# #  scripts            = [],      # Command Line callable scripts e.g. '/Path/To/Script.py'
#  packages             = find_packages(exclude=["tests*"]),
#  include_package_data = True,
#  zip_safe             = False,
#  test_suite           = "tests",
# #  tests_require      = ['tox'],
# #  cmdclass           = {'test': Tox}
#  command_options      = {'build_sphinx': {
#                             'version'    : ('setup.py', __meta__.version()),
#                             'release'    : ('setup.py', __meta__.release()),
#                             'source_dir' : ('setup.py','docs'),
#                             'build_dir'  : ('setup.py','dist'),
#                             'config_dir' : ('setup.py','docs'),}},
#  classifiers          = [ # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
#   "Development Status :: 2 - Pre-Alpha",
#   "Intended Audience :: Developers",
#   "Natural Language :: English",
#   "Operating System :: OS Independent",
#   "Programming Language :: Python",
#   "Programming Language :: Python :: 2",
#   "Programming Language :: Python :: 2.7",
#   "Programming Language :: Python :: 3",
#   "Programming Language :: Python :: 3.4",
#   "Programming Language :: Python :: 3.5",
#   "Programming Language :: Python :: 3.6",
#   "Topic :: Software Development :: Libraries :: Python Modules",
#   "Topic :: Utilities"
#  ],
# )
