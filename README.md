Sphinx : Nose Job
=================

This provides a handful of features one found useful when using Sphinx to document their source code.
Presently this includes the following features

 API-Doc
  An alternative to the sphinx API doc function that tries to support an output structure that maps more closely to the code base and eases maintenance.
  This was written to be backwards compatible with the original implementation.
 Epilog
  This provides a "universal" epilog for sphinx for authors working on multiple projects that need a common set of replacements.
  The intention is to provide a corresponding glossary module.
 