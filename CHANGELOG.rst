Refactor
--------------------------------------------------------------------------------
.. codeauthor:: Carel van Dam <carelvdam@gmail.com>
    :Date:2019-11-14T12:54:13+02:00

Decided to make the modules separately importable, hence :file:`database.py` is kept as a standalone module that one can readily extract.


Database
--------------------------------------------------------------------------------
.. codeauthor:: Carel van Dam <carelvdam@gmail.com>
    :Date:2019-11-14T12:28:26+02:00

Include the database directives from another package


Miscellaneous
--------------------------------------------------------------------------------
.. codeauthor:: Carel van Dam <carelvdam@gmail.com>
    :Date:2019-11-14T12:20:40+02:00

Minor edits


Path Trees
--------------------------------------------------------------------------------
.. codeauthor:: Carel van Dam <carelvdam@gmail.com>
    :Date:2019-11-14T11:54:48+02:00

This merges the `trees` directive, internally called `PathTrees`, from the original Nose Job package into `sphinxcontrib.nosejob`.
This is also a merge commit and pulls the 'master' branch into Nose Job.

 Conflicts:
	CHANGELOG.rst
	nosejob/__init__.py
	sphinxcontrib/nosejob/directives.py
	sphinxcontrib/nosejob/nodes.py


Trees
--------------------------------------------------------------------------------
.. codeauthor:: Carel van Dam <carelvdam@gmail.com>
    :Date:2019-11-14T12:06:10+02:00

Renamed trees.py to directives.py


Epilog
--------------------------------------------------------------------------------
.. codeauthor:: Carel van Dam <carelvdam@gmail.com>
    :Date:2019-11-14T10:39:01+02:00

Pulled in the Epilog code from the NoseJob branch


Documentation
--------------------------------------------------------------------------------
.. codeauthor:: Carel van Dam <carelvdam@gmail.com>
    :Date:2019-11-14T10:38:29+02:00

Updated the documentation so that it compiles again


PBR
--------------------------------------------------------------------------------
.. codeauthor:: Carel van Dam <carelvdam@gmail.com>
    :Date:2019-11-14T10:36:38+02:00

Removed the dependency upon PBR.


Miscellaneous Edits
--------------------------------------------------------------------------------
.. codeauthor:: Carel van Dam <carelvdam@gmail.com>
    :Date:2019-11-13T19:41:36+02:00

Updated the Git attributes and ignore files.
Included images used in the documentation.


Documentation
--------------------------------------------------------------------------------
.. codeauthor:: Carel van Dam <carelvdam@gmail.com>
    :Date:2019-11-13T18:55:42+02:00

Included the README.md and other documentation.


Git Configuration
--------------------------------------------------------------------------------
.. codeauthor:: Carel van Dam <carelvdam@gmail.com>
    :Date:2019-11-13T18:55:13+02:00

Included a Git attributes file


Unit Tests
--------------------------------------------------------------------------------
.. codeauthor:: Carel van Dam <carelvdam@gmail.com>
    :Date:2019-11-13T18:48:56+02:00

Included the unit tests for trees.


Trees
--------------------------------------------------------------------------------
.. codeauthor:: Carel van Dam <carelvdam@gmail.com>
    :Date:2019-11-13T18:47:16+02:00

Initial attempt at supporting trees within ones documentation


Saving State
--------------------------------------------------------------------------------
.. codeauthor:: Carel van Dam <carelvdam@gmail.com>
    :Date:2019-11-13T18:30:25+02:00

This is to save the state of the application.


Updated the documentation
--------------------------------------------------------------------------------
.. codeauthor:: Carel van Dam <carelvdam@gmail.com>
    :Date:2019-04-22T23:13:37+02:00

Documented the available testing methodologies.


Initial commit
--------------------------------------------------------------------------------
.. codeauthor:: Carel van Dam <carelvdam@gmail.com>
    :Date:2019-04-21T15:06:04+02:00

This provides the initial code for Nose Job. Strictly speaking this repository supersedes a predecessor with the same name.

