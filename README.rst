=====================
sphinxcontrib-nosejob
=====================

.. image:: https://travis-ci.org/sphinx-contrib/sphinxcontrib-nosejob.svg?branch=master
    :target: https://travis-ci.org/sphinx-contrib/sphinxcontrib-nosejob

A collection of directives to compliment those provided by `Sphinx <https://github.com/sphinx-doc/sphinx>`_

Overview
--------

This provides set of directives to the user for use in their own documentation.

Versioning
----------

I'm not sure what the best method is for determining `version <https://stackoverflow.com/questions/20180543/how-to-check-version-of-python-modules>`_ `information <https://packaging.python.org/guides/single-sourcing-package-version/>`_.
`SCM <https://pypi.org/project/setuptools-scm/>`_ seems to be the better method.

Links
-----

- Source: https://github.com/sphinx-contrib/sphinxcontrib-nosejob
- Bugs: https://github.com/sphinx-contrib/sphinxcontrib-nosejob/issues
