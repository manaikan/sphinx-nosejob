"""
    pytest config for sphinxcontrib/nosejob/tests
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"""

pytest_plugins = 'sphinx.testing.fixtures'
