.. Common Terms

.. |Docutils| replace:: Docutils

.. |Python|   replace:: Python

.. |Sphinx|   replace:: Sphinx

.. |Jinja|    replace:: Jinja

.. Formats 

.. - Hyper Text Mark up Language

.. |HTML|     replace:: Hyper Text Mark up Language

.. - Mark Down

.. |MD| replace:: Mark Down

.. - ReStructured Text

.. |rst| replace:: restructured text
.. |RsT| replace:: Restructured Text
.. |RST| replace:: ReStructured Text

.. - XML

.. |XML| replace:: eXtensible Markup Language

.. - ODT

.. |ODT|   replace:: Open Document T?

.. - PDF

.. |PDF|   replace:: Portable Document Format

.. - LaTeX

.. |TeXs|  replace:: TeX and Friends

.. |TeX|   replace:: TeX

.. |LaTeX| replace:: LaTeX

